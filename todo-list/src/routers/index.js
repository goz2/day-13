import {createBrowserRouter} from 'react-router-dom'
import HelpPage from '../pages/HelpPage'
import Error404Page from '../pages/Error404Page'
import App from '../App'
import DoneListPage from '../pages/DoneListPage'
import HomeTodoPage from '../pages/HomeTodoPage'
import DoneTodoDetail from '../pages/DoneTodoDetail'
export const router = createBrowserRouter([
    {
        path: '/',
        element: <App></App>,
        children: [
            {
                index: true,
                element: <HomeTodoPage></HomeTodoPage>
            },
            {
                path: '/help',
                element: <HelpPage></HelpPage>
            },
            {
                path: '/done',
                element: <DoneListPage></DoneListPage>
            },
            {
                path: '/done/:id',
                element: <DoneTodoDetail></DoneTodoDetail>
            }
        ]
    },
    {
        path: '*',
        element: <Error404Page></Error404Page>
    }
])

 